# samAccountName,Manager,StreetAddress
# bob,"CN=Alice,OU=Users,DC=ad,DC=example,DC=com",33 Richardson Street

[CmdletBinding()]
Param(
    [Parameter(Mandatory = $true)]
    [ValidateNotNullOrEmpty()]
    [String]$inputCsv
)

$ErrorActionPreference = "Stop"

Import-Module ActiveDirectory -ErrorAction Stop

if (-not (Test-Path -Path $inputCsv)) {
    Write-Warning "${inputCsv}: no such file"
    Exit 1
}

foreach ($user in (Import-Csv -Path $inputCsv)) {
    $samAccountName = $user.SamAccountName
    $manager = $user.Manager
    $streetAddress = $user.StreetAddress

    if (-not (Get-ADUser -Filter {SamAccountName -eq $SamAccountName} -ErrorAction SilentlyContinue)) {
        Write-Warning "$SamAccountName does not exist"
        continue
    }

    if (-not (Get-ADObject -Filter {DistinguishedName -eq $manager} -ErrorAction SilentlyContinue)) {
    Write-Warning "${SamAccountName}: manager attribute is invalid"
        continue
    }

    Set-ADUser -Identity $samAccountName -StreetAddress $StreetAddress -Manager $Manager
    Write-Host -ForegroundColor Green "$SamAccountName edited"
}
