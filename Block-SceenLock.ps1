[CmdletBinding()]
Param(
    [Parameter()]
    [ValidateRange(1,1440)]
    [Int]$Minutes = 60
)

$WShell = New-Object -Com "Wscript.Shell"

for ($i = 0; $i -lt $Minutes; $i++) {
	$WShell.SendKeys("{SCROLLLOCK}")
	sleep 60
}