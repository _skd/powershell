$Uri = 'http://worldclockapi.com/api/json/utc/now'

$WorldClockTime = [datetime](Invoke-RestMethod -UseBasicParsing -Uri $Uri).currentDateTime
$LocalTime = Get-Date

$TimeSpan = New-TimeSpan -Start $WorldClockTime -End $LocalTime
Write-Output "Time out by: $($TimeSpan.Minutes) minutes"