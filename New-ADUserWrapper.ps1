# DisplayName,UserPrincipalName,ProxyAddresses,Password
# Charlie Root,user@example.com,SMTP:user@example.com+smtp:user@example.org,Password01

[CmdletBinding()]
Param(
    [Parameter(Mandatory = $true)]
    [ValidateNotNullOrEmpty()]
    [String]$inputCsv,

    [Parameter(Mandatory = $true)]
    [ValidateNotNullOrEmpty()]
    [String]$ouPath
)

$ErrorActionPreference = "Stop"

Import-Module ActiveDirectory -ErrorAction Stop

if (-not (Test-Path -Path $inputCsv)) {
    Write-Warning "${inputCsv}: no such file"
    exit 1
}

if (-not (Get-ADOrganizationalUnit -Filter "DistinguishedName -eq '$ouPath'")) {
    Write-Warning  "${ouPath}: does not exist."
    exit 1
}

foreach ($user in (Import-Csv -Path $inputCsv)) {
    $samAccountName = $user.UserPrincipalName.Split("@")[0]
    if (Get-ADUser -Filter {SamAccountName -eq $samAccountName} -ErrorAction SilentlyContinue) {
        Write-Warning "$samAccountName exists"
        continue
    }

    $displayName = $user.DisplayName
    $userPrincipalName = $user.UserPrincipalName
    $proxyAddresses = $user.proxyAddresses -split ";"
    $password = $user.Password

    New-ADUser -SamAccountName $samAccountName `
               -UserPrincipalName $userPrincipalName `
               -Name $displayName `
               -DisplayName $displayName `
               -Enabled $true `
               -Path $ouPath `
               -AccountPassword (ConvertTo-SecureString -String $password -AsPlainText -Force)

    Set-ADUser -Identity $samAccountName -Add @{proxyAddresses = $proxyAddresses}
    Set-ADUser -Identity $samAccountName -ChangePasswordAtLogon $true
    Write-Host -ForegroundColor Green "$userPrincipalName created"
}
