[CmdletBinding()]
Param(
    [Parameter(Mandatory = $true)]
    [ValidateSet('Computer','User')] 
    [String]$Object,

    [Parameter(Mandatory = $true)]
    [ValidateSet('Active','Inactive')] 
    [String]$Policy,

    [Parameter(Mandatory = $true)]
    [ValidateRange(1,9999)]
    [Int]$Days
)

Import-Module ActiveDirectory -ErrorAction Stop

$Time = (Get-Date).Adddays(-($Days))
switch ($Object)
{
    "Computer"
    {
        switch ($Policy)
        {
            "Active"   { Get-ADComputer -Filter { LastLogonDate -ge $Time -and Enabled -eq $true } -Properties * | Sort-Object Name | Select-Object Name,LastLogonDate; break }
            "Inactive" { Get-ADComputer -Filter { LastLogonDate -le $Time -and Enabled -eq $true } -Properties * | Sort-Object Name | Select-Object Name,LastLogonDate; break }
        }
    }
    "User"
    {
        switch ($Policy)
        {
            "Active"   { Get-ADUser -Filter { LastLogonDate -ge $Time -and Enabled -eq $true } -Properties * | Sort-Object Name | Select-Object Name,LastLogonDate; break }
            "Inactive" { Get-ADUser -Filter { LastLogonDate -le $Time -and Enabled -eq $true } -Properties * | Sort-Object Name | Select-Object Name,LastLogonDate; break }
        }
    }
}
