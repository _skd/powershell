function Send-VBRPushoverAlert
{
    [CmdletBinding()]
    Param(
        [Parameter(Mandatory = $true)]
        [AllowEmptyString()]
        [String]$Message
    )

    if ([string]::IsNullOrWhiteSpace($Message)) {
        $Message = "failed to set message parameter"
    }

    $Uri = "https://api.pushover.net/1/messages.json"
    $Body = @{
        token   = "";
        user    = "";
        title   = "${env:COMPUTERNAME}: [$($LastBackup.Result)] $($LastBackup.Name)";
        message = "$Message";
    }
    Invoke-RestMethod -Method Post -Uri $Uri -Body $Body | Out-Null
}

function Get-VBRBackupStatus
{
    [CmdletBinding()]
    Param(
        [Parameter(Mandatory = $true)]
        [ValidateNotNullOrEmpty()]
        [String]$Status
    )

    $LastBackup.Logger.GetLog().UpdatedRecords | Where-Object {$_.Status -eq $Status} | Select-Object -ExpandProperty Title | Out-String
}

$LastBackup = Get-VBRBackupSession | Sort-Object -Property EndTime | Select-Object -Last 1
switch ($LastBackup.Result)
{
    "Failed"  { Send-VBRPushoverAlert -Message $(Get-VBRBackupStatus -Status "EFailed"); break }
    "Warning" { Send-VBRPushoverAlert -Message $(Get-VBRBackupStatus -Status "EWarning"); break }
}
