<#
.Synopsis
    Create default Active Directory organization units structure

.DESCRIPTION
    Create default Active Directory organization units structure

        <Company Name>
            <City Name>
                Computers
                    Active Computers
                    Disabled Computers
                Groups
                    Distribution Groups
                    Security Groups
                        Rights
                        Roles
                No365Sync
                Servers
                    Active Servers
                        Member Servers
                        Remote Desktop Session Hosts
                    Disabled Servers
                Users
                    Active Accounts
                    Disabled Accounts
                    Service Accounts
                    Support Accounts

.PARAMETER CompanyName
    Specify the company name

.PARAMETER CityName
    Specify the city name

.EXAMPLE
    PS> Create-DefaultADOrganizationalUnits -CompanyName "City17" -CityName "Perth"

.NOTES
    Version 0.1
#>

[CmdletBinding()]
Param(
    [Parameter(Mandatory = $true)]
    [ValidateNotNullOrEmpty()]
    [string]$CompanyName,

    [Parameter(Mandatory = $true)]
    [ValidateNotNullOrEmpty()]
    [string]$CityName
)

$ErrorActionPreference = "Stop"

Import-Module ActiveDirectory

$rootDN = (Get-ADDomain).DistinguishedName
$CompanyName = "0. $CompanyName"

New-ADOrganizationalUnit -Name "$CompanyName" -Path "$rootDN"
New-ADOrganizationalUnit -Name "$CityName" -Path "OU=$CompanyName,$rootDN"

# Computers
New-ADOrganizationalUnit -Name "Computers" -Path "OU=$CityName,OU=$CompanyName,$rootDN"
New-ADOrganizationalUnit -Name "Active Computers" -Path "OU=Computers,OU=$CityName,OU=$CompanyName,$rootDN"
New-ADOrganizationalUnit -Name "Disabled Computers" -Path "OU=Computers,OU=$CityName,OU=$CompanyName,$rootDN"

# Groups
New-ADOrganizationalUnit -Name "Groups" -Path "OU=$CityName,OU=$CompanyName,$rootDN"
New-ADOrganizationalUnit -Name "Distribution Groups" -Path "OU=Groups,OU=$CityName,OU=$CompanyName,$rootDN"
New-ADOrganizationalUnit -Name "Security Groups" -Path "OU=Groups,OU=$CityName,OU=$CompanyName,$rootDN"
New-ADOrganizationalUnit -Name "Rights" -Path "OU=Security Groups,OU=Groups,OU=$CityName,OU=$CompanyName,$rootDN"
New-ADOrganizationalUnit -Name "Roles" -Path "OU=Security Groups,OU=Groups,OU=$CityName,OU=$CompanyName,$rootDN"

# No365Sync
New-ADOrganizationalUnit -Name "No365Sync" -Path "OU=$CityName,OU=$CompanyName,$rootDN"

# Servers
New-ADOrganizationalUnit -Name "Servers" -Path "OU=$CityName,OU=$CompanyName,$rootDN"
New-ADOrganizationalUnit -Name "Active Servers" -Path "OU=Servers,OU=$CityName,OU=$CompanyName,$rootDN"
New-ADOrganizationalUnit -Name "Member Servers" -Path "OU=Active Servers,OU=Servers,OU=$CityName,OU=$CompanyName,$rootDN"
New-ADOrganizationalUnit -Name "Remote Desktop Session Hosts" -Path "OU=Active Servers,OU=Servers,OU=$CityName,OU=$CompanyName,$rootDN"

# Users
New-ADOrganizationalUnit -Name "Users" -Path "OU=$CityName,OU=$CompanyName,$rootDN"
New-ADOrganizationalUnit -Name "Active Accounts" -Path "OU=Users,OU=$CityName,OU=$CompanyName,$rootDN"
New-ADOrganizationalUnit -Name "Disabled Accounts" -Path "OU=Users,OU=$CityName,OU=$CompanyName,$rootDN"
New-ADOrganizationalUnit -Name "Service Accounts" -Path "OU=Users,OU=$CityName,OU=$CompanyName,$rootDN"
New-ADOrganizationalUnit -Name "Support Accounts" -Path "OU=Users,OU=$CityName,OU=$CompanyName,$rootDN"
