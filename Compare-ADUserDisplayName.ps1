# DisplayName
# Charlie Root

[CmdletBinding()]
Param(
    [Parameter(Mandatory = $true)]
    [ValidateNotNullOrEmpty()]
    [String]$inputCsv
)

$ErrorActionPreference = "Stop"

Import-Module ActiveDirectory -ErrorAction Stop

if (-not (Test-Path -Path $inputCsv)) {
    Write-Warning "${inputCsv}: no such file"
    exit 1
}

foreach ($user in (Import-Csv -Path $inputCsv)) {
    $DisplayName = $user.DisplayName

    # $DisplayName cannot be escaped inline
    $EscapedDisplayName = "`"$DisplayName`""
    $DisplayNameFound = Get-ADUser -Filter "DisplayName -eq $EscapedDisplayName"

    if ($DisplayNameFound) {
        Write-Host -ForegroundColor Green "valid: $DisplayName"
    } else {
        Write-Host -ForegroundColor Red "invalid: $DisplayName"
    }
}
