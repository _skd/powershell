$ErrorActionPreference = "Stop"

Import-Module ADSync

# Assign the local AD and Azure AD connectors value
$adConnector = (Get-ADSyncConnector | Where-Object { $_.Type -eq "AD" }).Name
$aadConnector = (Get-ADSyncConnector | Where-Object { $_.Type -eq "Extensible2" }).Name

if ([string]::IsNullOrWhiteSpace($adConnector) -or [string]::IsNullOrWhiteSpace($aadConnector)) {
    Write-Error "failed to fetch Get-ADSyncConnector connector values"
	Exit
}

# Create a new ForceFullPasswordSync configuration parameter object
$c = Get-ADSyncConnector -Name $adConnector

# Update the existing connector with the following new configuration
$p = New-Object Microsoft.IdentityManagement.PowerShell.ObjectModel.ConfigurationParameter "Microsoft.Synchronize.ForceFullPasswordSync",String,ConnectorGlobal,$null,$null,$null
$p.Value = 1
$c.GlobalParameters.Remove($p.Name)
$c.GlobalParameters.Add($p)
$c = Add-ADSyncConnector -Connector $c

# Disable Azure AD Connect
Set-ADSyncAADPasswordSyncConfiguration -SourceConnector $adConnector -TargetConnector $aadConnector -Enable $false

# Re-enable Azure AD Connect to force a full password synchronization
Set-ADSyncAADPasswordSyncConfiguration -SourceConnector $adConnector -TargetConnector $aadConnector -Enable $true
