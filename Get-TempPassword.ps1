[CmdletBinding()]
Param(
    [Parameter()]
    [ValidateRange(1,99)]
    [Int]$Passwords = 1
)

$Url = "https://dinopass.com/password/strong"
for ($i = 0; $i -lt $Passwords; $i++) {
    $WebResponse = Invoke-WebRequest $Url -UseBasicParsing -ErrorAction Stop
    $WebResponse.Content
}
