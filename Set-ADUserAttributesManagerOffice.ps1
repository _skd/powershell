# UserDisplayName,ManagerDisplayName,Office
# Tim Canterbury,David Brent,Wernham Hogg

[CmdletBinding()]
Param(
    [Parameter(Mandatory = $true)]
    [ValidateNotNullOrEmpty()]
    [String]$InputCsv,

    [Parameter(Mandatory = $false)]
    [Switch]$Test
)

$ErrorActionPreference = "Stop"

function Check-ADDisplayName
{
    Param(
        [Parameter(Mandatory = $true)]
        [ValidateNotNullOrEmpty()]
        [String]$DisplayName,

        [Parameter(Mandatory = $true)]
        [ValidateNotNullOrEmpty()]
        [String]$UserType
    )

    $DisplayNameFound = Get-ADUser -Filter "DisplayName -eq '$DisplayName'"

    if ($Test -eq $false) {
        if (!$DisplayNameFound) {
            $global:DisplayNameError = $true
        }
    } else {
        $DisplayName = $DisplayName.Replace("''", "'")
        if ($DisplayNameFound) {
            Write-Host -ForegroundColor Green "${UserType}DisplayName: $DisplayName is valid"
        } else {
            Write-Host -ForegroundColor Red "${UserType}DisplayName: $DisplayName is invalid"
            return $false
        }
    }
    return $true
}

Import-Module ActiveDirectory

if (-not (Test-Path -Path $InputCsv)) {
    Write-Warning "${InputCsv}: no such file"
    exit 1
}

foreach ($user in (Import-Csv -Path $InputCsv)) {
    $UserDisplayName = $user.UserDisplayName
    $UserDisplayNameEscaped = $UserDisplayName.Replace("'", "''")
    if (Check-ADDisplayName -DisplayName $UserDisplayNameEscaped -UserType User) {
        $UserName = Get-ADUser -Filter "DisplayName -eq '$UserDisplayNameEscaped'" -ErrorAction SilentlyContinue
    }

    $ManagerDisplayName = $user.ManagerDisplayName
    $ManagerdisplayNameEscaped = $ManagerDisplayName.Replace("'", "''")
    if (Check-ADDisplayName -DisplayName $ManagerdisplayNameEscaped -UserType Manager) {
        $Manager = Get-ADUser -Filter "DisplayName -eq '$ManagerdisplayNameEscaped'" -ErrorAction SilentlyContinue
    }

    if ($Test -eq $false) {
        if ($global:DisplayNameError -eq $false) {
            $UsersAMAccountName = $UserName.sAMAccountName
            $ManagersAMAccountName = $Manager.sAMAccountName
            $Office = $user.Office

            try {
                Set-ADUser -Identity $UsersAMAccountName -Manager $ManagersAMAccountName -Office $Office
                Write-Host -ForegroundColor Green "UPDATED: $UserDisplayName"
            } catch {
                Write-Host -ForegroundColor Red "FAILED:  $UserDisplayName (Set-ADUser error)"
            }
        } else {
            Write-Host -ForegroundColor Red "FAILED:  $UserDisplayName (DisplayName error)"
        }
        $global:DisplayNameError = $false
    }
}

